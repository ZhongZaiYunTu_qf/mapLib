# mapLib

#### 介绍
地图组件

#### 版本
[![](https://jitpack.io/v/com.gitee.manypeaks/mapLib.svg)](https://jitpack.io/#com.gitee.manypeaks/mapLib)


#### 安装教程
1  加上网址

        allprojects {
            repositories {
                ...
                maven { url 'https://jitpack.io' }
            }
	}

2  使用

        dependencies {
            //地图
	    implementation 'com.gitee.ZhongZaiYunTu_qf:mapLib:1.1'
	}

#### 使用说明

1.  跳转到地图选择点

1.1 跳转到地图选择界面
     
            ActivityUtils.startActivityForResult(this, LocationSelectionActivity.class, 123);

1.2 返回选择点数据

            @Override
            public void onActivityResult(int requestCode, int resultCode, Intent data) {
                super.onActivityResult(requestCode, resultCode, data);
                if (resultCode == Activity.RESULT_OK) {
                    MapListEntity  mapBean = (MapListEntity) data.getSerializableExtra(BundleKey.ENTITY.name());
                     LatteLogger.json(GsonUtils.toJson(mapBean));
                }
            }
    
2.  绘制地图界面
    
2.1 添加地址

            ActivityUtils.startActivity(AddressAddedActivity.class);
    

#### 参与贡献

