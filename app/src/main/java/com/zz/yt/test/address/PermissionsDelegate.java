package com.zz.yt.test.address;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.blankj.utilcode.util.ToastUtils;
import com.zz.yt.lib.map.delegate.draw.MapViewDelegate;
import com.zz.yt.lib.ui.base.LatteDelegate;
import com.zz.yt.test.R;


public class PermissionsDelegate extends LatteDelegate {

    @NonNull
    public static PermissionsDelegate create() {
        Bundle args = new Bundle();
        PermissionsDelegate fragment = new PermissionsDelegate();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected Object setLayout() {
        return R.layout.delegate_main;
    }

    @Override
    protected void onBindView(@Nullable Bundle savedInstanceState,
                              @NonNull View rootView) {
        starClickWithCheck(index -> ToastUtils.showShort("权限获取成功"));
        findViewById(R.id.id_text_name).setOnClickListener(v -> {
            // 开始绘制
            start(MapViewDelegate.create());
        });
    }

}
