package com.zz.yt.test.address;

import android.os.Bundle;
import android.widget.Button;

import androidx.annotation.Nullable;

import com.amap.api.maps.CameraUpdateFactory;
import com.amap.api.services.core.LatLonPoint;
import com.zz.yt.lib.map.base.BaseMapFragmentActivity;
import com.zz.yt.lib.map.utils.AMapUtil;
import com.zz.yt.lib.ui.CustomTitleBar;

/**
 * 路径规划
 *
 * @author qf
 * @version 1.0.3
 */
public class PlanningActivity extends BaseMapFragmentActivity {

    private final LatLonPoint mStartPoint = new LatLonPoint(39.942295, 116.335891);//起点，116.335891,39.942295
    private final LatLonPoint mEndPoint = new LatLonPoint(39.995576, 116.481288);//终点，116.481288,39.995576

    @Override
    protected void setCustomTitleBar(CustomTitleBar titleBar) {
        if (titleBar != null) {
            titleBar.setText("路径规划");
        }
    }

    @Override
    protected Object setLayout() {
        return com.zz.yt.lib.map.R.layout.hai_map_delegate_base_map;
    }

    @Override
    public void onBindView(@Nullable Bundle savedInstanceState) {
        setUpMapIfNeeded();
        initView();
    }

    private void initView() {
        Button finish = findViewById(com.zz.yt.lib.map.R.id.id_btn_submit);
        if (finish != null) {
            finish.setText("路径规划");
            finish.setOnClickListener(v -> setPlanningDrive(mStartPoint, mEndPoint));
        }
        if (mMap != null) {
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(AMapUtil.convertToLatLng(mStartPoint), 16));
        }
    }
}
