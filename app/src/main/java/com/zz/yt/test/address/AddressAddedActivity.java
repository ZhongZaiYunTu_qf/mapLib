package com.zz.yt.test.address;

import android.annotation.SuppressLint;
import android.content.Intent;

import com.blankj.utilcode.util.GsonUtils;
import com.whf.android.jar.base.delegate.BaseDelegate;
import com.whf.android.jar.base.latte.BaseProxyActivity;
import com.whf.android.jar.util.log.LatteLogger;
import com.zz.yt.lib.map.delegate.address.added.AddressAddedDelegate;
import com.zz.yt.lib.map.delegate.address.entity.AddressEntity;
import com.zz.yt.lib.map.delegate.selection.entity.MapListEntity;
import com.zz.yt.lib.ui.BundleKey;


/***
 * 新建收货地址
 * @author qf
 * @version 1.0
 */
@SuppressLint("NonConstantResourceId")
public class AddressAddedActivity extends BaseProxyActivity {

    @Override
    public BaseDelegate setRootDelegate() {
        return AddressAddedDelegate.create(new AddressEntity());
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null) {
            MapListEntity mapListEntity = (MapListEntity) data.getExtras().getSerializable(BundleKey.ENTITY.name());
            if (mapListEntity != null) {
                LatteLogger.json(GsonUtils.toJson(mapListEntity));
            }
        }
    }
}
