package com.zz.yt.test;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.blankj.utilcode.util.ActivityUtils;
import com.blankj.utilcode.util.ColorUtils;
import com.blankj.utilcode.util.LogUtils;
import com.whf.android.jar.util.log.LatteLogger;
import com.zz.yt.lib.map.MapUtils;
import com.zz.yt.lib.map.colorpicker.ColorPickUtils;
import com.zz.yt.lib.map.delegate.address.added.AddressAddedActivity;
import com.zz.yt.lib.map.delegate.selection.LocationSelectionActivity;
import com.zz.yt.lib.map.delegate.draw.MapFragmentActivity;
import com.zz.yt.lib.map.delegate.edit.EditDrawActivity;
import com.zz.yt.lib.map.delegate.weather.WeatherActivity;
import com.zz.yt.lib.map.utils.AMapUpUtils;
import com.zz.yt.lib.map.utils.LocationUtils;
import com.zz.yt.test.address.InputTipsActivity;
import com.zz.yt.test.address.PermissionsActivity;
import com.zz.yt.test.address.PlanningActivity;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Locale;


/**
 * @author qf
 */
public class MainActivity extends Activity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        MapUtils.initApp();

        LogUtils.d("sha1=" + sHA1(getApplicationContext()));
    }

    /**
     * @param view:设置权限
     */
    public void onClick(View view) {
        ActivityUtils.startActivity(PermissionsActivity.class);
    }

    /**
     * @param view:获取位置
     */
    public void onClick0(View view) {
        ActivityUtils.startActivity(LocationSelectionActivity.class);
    }

    /**
     * @param view:开始绘制
     */
    public void onClick1(View view) {
        ActivityUtils.startActivity(MapFragmentActivity.class);
    }

    public void onClick2(View view) {
        ActivityUtils.startActivity(EditDrawActivity.class);
    }

    /**
     * @param view:添加地址
     */
    public void onClick3(View view) {
        //添加地址
        ActivityUtils.startActivity(AddressAddedActivity.class);
    }

    /**
     * @param view:颜色选择
     */
    public void onClick4(View view) {
        ColorPickUtils.create(this, color -> {
            if (view != null) {
                view.setBackgroundColor(color);
                LatteLogger.i(ColorUtils.int2RgbString(color) + "，" + ColorUtils.int2ArgbString(color));
            }
        });
    }

    /**
     * @param view:天气
     */
    public void onClick5(View view) {
        ActivityUtils.startActivity(WeatherActivity.class);
    }

    public void onClick6(View view) {
        AMapUpUtils.getInstance().setUrl("").setPermissions();
    }

    /**
     * @param view:导航
     */
    public void onClick7(View view) {
        ActivityUtils.startActivity(PlanningActivity.class);
    }

    public void onClick8(View view) {
        //输入文字定位
        ActivityUtils.startActivity(InputTipsActivity.class);
    }

    @Nullable
    public static String sHA1(@NonNull Context context) {
        try {
            @SuppressLint("PackageManagerGetSignatures")
            PackageInfo info = context.getPackageManager().getPackageInfo(
                    context.getPackageName(), PackageManager.GET_SIGNATURES);
            byte[] cert = info.signatures[0].toByteArray();
            MessageDigest md = MessageDigest.getInstance("SHA1");
            byte[] publicKey = md.digest(cert);
            StringBuilder hexString = new StringBuilder();
            for (byte b : publicKey) {
                String appendString = Integer.toHexString(0xFF & b)
                        .toUpperCase(Locale.US);
                if (appendString.length() == 1)
                    hexString.append("0");
                hexString.append(appendString);
                hexString.append(":");
            }
            String result = hexString.toString();
            return result.substring(0, result.length() - 1);
        } catch (PackageManager.NameNotFoundException | NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocationUtils.getInstance().onDestroy();
    }

}
