package com.zz.yt.test;

import android.app.Application;

import com.whf.android.jar.app.Latte;
import com.whf.android.jar.net.HttpCode;

/**
 * @author qf
 * @version  2020-04-16
 */
public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        //全局初始化
        Latte.init(this)
                .withGuide(3)
                .withGuide(false)
                .withLoaderDelayed(1000)
                .withAdaptation(360f)
                .withApiHost("http://www.baidu.com")
                .withFtpHost("http://www.baidu.com")
                .withWebHost("http://www.baidu.com")
                .withJavascriptInterface("http://www.baidu.com")
                .withLoggable(BuildConfig.DEBUG)
                .withVersionCode(BuildConfig.VERSION_CODE)
                .withVersionName(BuildConfig.VERSION_NAME)
                .withApplicationId(BuildConfig.APPLICATION_ID)
                .withSignExpiration(HttpCode.CODE_403)
                .withInterceptor()
                .configure();
    }
}
