package com.zz.yt.test.address;

import com.whf.android.jar.base.delegate.BaseDelegate;
import com.whf.android.jar.base.latte.BaseProxyActivity;
import com.zz.yt.lib.map.delegate.input.InputTipsDelegate;

/**
 * 输入文字定位
 *
 * @author qf
 * @version 1.0
 */
public class InputTipsActivity extends BaseProxyActivity {

    @Override
    public BaseDelegate setRootDelegate() {
        return InputTipsDelegate.create();
    }


}
