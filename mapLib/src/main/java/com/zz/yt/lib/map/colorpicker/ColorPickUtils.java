package com.zz.yt.lib.map.colorpicker;

import android.annotation.SuppressLint;
import android.content.Context;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.core.CenterPopupView;
import com.zz.yt.lib.map.R;


public class ColorPickUtils {


    public static void create(Context context, OnColorBarListener listener) {
        new ColorPickUtils(context, listener);
    }

    private ColorPickUtils(Context context, OnColorBarListener listener) {
        new XPopup.Builder(context)
                .moveUpToKeyboard(false)
                .enableDrag(true)
                .isDestroyOnDismiss(false)
                .isThreeDrag(true)
                .asCustom(new ColorPickPopup(context, listener))
                .show();
    }


    private static class ColorPickPopup extends CenterPopupView {
        private TextView textShow = null;
        private final OnColorBarListener mColorBarListener;

        public ColorPickPopup(@NonNull Context context, OnColorBarListener listener) {
            super(context);
            this.mColorBarListener = listener;
        }

        @Override
        protected int getImplLayoutId() {
            return R.layout.hai_map_popup_pick_color;
        }

        @SuppressLint("SetTextI18n")
        @Override
        protected void onCreate() {
            super.onCreate();
            textShow = findViewById(R.id.map_text_show);
            final ColorPickView pickView = findViewById(R.id.map_color_pick);
            if (pickView != null) {
                pickView.setBarListener(new OnColorBarListener() {
                    @Override
                    public void moveBar(int color) {
                        if (textShow != null) {
                            textShow.setBackgroundColor(color);
                        }
                        if (mColorBarListener != null) {
                            mColorBarListener.moveBar(color);
                        }
                    }
                });
            }

        }
    }

}
