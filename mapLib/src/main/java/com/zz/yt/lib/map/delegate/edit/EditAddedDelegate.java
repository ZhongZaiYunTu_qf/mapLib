package com.zz.yt.lib.map.delegate.edit;

import com.zz.yt.lib.map.R;
import com.zz.yt.lib.ui.CustomTitleBar;
import com.zz.yt.lib.ui.base.ui.LatteTitleDelegate;


/**
 * 编辑网格
 */
public class EditAddedDelegate extends LatteTitleDelegate {

    @Override
    protected void setCustomTitleBar(CustomTitleBar titleBar) {
        if (titleBar != null) {
            titleBar.setText("编辑网格");
        }
    }

    @Override
    protected Object setLayout() {
        return  R.layout.hai_map_delegate_ui_edit_added;
    }

}
