package com.zz.yt.lib.map.delegate.address.entity;


import java.io.Serializable;

public class AddressEntity implements Serializable {


    private String mConsignee;
    private String mTelephone;
    private String mRegion;
    private String mAddress;
    private boolean isDefault;

    public String getConsignee() {
        return mConsignee;
    }

    public void setConsignee(String mConsignee) {
        this.mConsignee = mConsignee;
    }

    public String getTelephone() {
        return mTelephone;
    }

    public void setTelephone(String mTelephone) {
        this.mTelephone = mTelephone;
    }

    public String getRegion() {
        return mRegion;
    }

    public void setRegion(String mRegion) {
        this.mRegion = mRegion;
    }

    public String getAddress() {
        return mAddress;
    }

    public void setAddress(String mAddress) {
        this.mAddress = mAddress;
    }

    public boolean isDefault() {
        return isDefault;
    }

    public void setDefault(boolean aDefault) {
        isDefault = aDefault;
    }
}
