package com.zz.yt.lib.map.delegate.edit;


import com.whf.android.jar.base.delegate.BaseDelegate;
import com.whf.android.jar.base.latte.BaseProxyActivity;


/**
 * 编辑网格
 */
public class EditDrawActivity extends BaseProxyActivity {


    @Override
    public BaseDelegate setRootDelegate() {
//        return new MapFragmentDelegate();
        return new EditAddedDelegate();
    }
//
//    @Override
//    protected void onCreate(@Nullable Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        //setContentView(R.layout.hai_map_delegate_ui_edit);
//    }
}
