package com.zz.yt.lib.map.delegate.selection.entity;

import java.io.Serializable;

public class MapListEntity implements Serializable {

    /**
     * 名字
     */
    private String name;
    /**
     * 距离
     */
    private String distance;
    /**
     * 所在地区
     */
    private String region;
    /**
     * 地址
     */
    private String address;
    private double latitude;
    private double longitude;
    /**
     * 是否
     */
    private boolean isSelect;


    public MapListEntity() {

    }

    public MapListEntity(String name, String distance, String region, String address, double latitude, double longitude) {
        this.name = name;
        this.distance = distance;
        this.region = region;
        this.address = address;
        this.latitude = latitude;
        this.longitude = longitude;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public boolean isSelect() {
        return isSelect;
    }

    public void setSelect(boolean select) {
        isSelect = select;
    }

}


