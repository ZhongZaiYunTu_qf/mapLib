package com.zz.yt.lib.map.base;


import android.annotation.TargetApi;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Build;

import com.amap.api.location.AMapLocation;
import com.amap.api.location.AMapLocationClient;
import com.amap.api.maps.AMap;
import com.amap.api.maps.CameraUpdateFactory;
import com.amap.api.maps.CoordinateConverter;
import com.amap.api.maps.MapFragment;
import com.amap.api.maps.MapsInitializer;
import com.amap.api.maps.UiSettings;
import com.amap.api.maps.model.BitmapDescriptor;
import com.amap.api.maps.model.BitmapDescriptorFactory;
import com.amap.api.maps.model.LatLng;
import com.amap.api.maps.model.Marker;
import com.amap.api.maps.model.MarkerOptions;
import com.amap.api.maps.model.MyLocationStyle;
import com.amap.api.maps.model.Polygon;
import com.amap.api.maps.model.PolygonOptions;
import com.amap.api.maps.model.Polyline;
import com.amap.api.maps.model.PolylineOptions;
import com.amap.api.services.core.AMapException;
import com.amap.api.services.core.LatLonPoint;
import com.amap.api.services.route.BusRouteResult;
import com.amap.api.services.route.DrivePath;
import com.amap.api.services.route.DriveRouteResult;
import com.amap.api.services.route.RideRouteResult;
import com.amap.api.services.route.RouteSearch;
import com.amap.api.services.route.WalkRouteResult;
import com.blankj.utilcode.util.ToastUtils;
import com.whf.android.jar.util.log.LatteLogger;
import com.zz.yt.lib.map.R;
import com.zz.yt.lib.map.checker.MapManager;
import com.zz.yt.lib.map.checker.IMapChecker;
import com.zz.yt.lib.map.delegate.draw.properties.DrawPolylineProperties;
import com.zz.yt.lib.map.delegate.draw.properties.DrawPolygonProperties;
import com.zz.yt.lib.map.driveroute.DrivingRouteOverlay;
import com.zz.yt.lib.map.utils.AMapUtil;
import com.zz.yt.lib.map.utils.LocationUtils;
import com.zz.yt.lib.ui.base.ui.LatteTitleActivity;

import java.util.List;


/**
 * 基本地图（MapFragment）实现
 *
 * @author qf
 * @version 1.0
 */
public abstract class BaseMapFragmentActivity extends LatteTitleActivity implements LocationUtils.OnLocationListener, RouteSearch.OnRouteSearchListener {

    protected AMap mMap;
    protected RouteSearch mRouteSearch;

    @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();
    }

    /**
     * 获取AMap 对象
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    protected void setUpMapIfNeeded() {
        update();
        if (mMap == null) {
            mMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
        }
        try {
            mRouteSearch = new RouteSearch(this);
            mRouteSearch.setRouteSearchListener(this);
        } catch (AMapException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //关闭后台定位通知
        LocationUtils.getInstance().disableNotification();
    }

    /**
     * 定位(默认16)
     *
     * @param zoomTo:缩放地图到指定的缩放级别,一共分为 17 级，从 3 到 19。数字越大，展示的图面信息越精细。
     */
    protected void initLocation(int zoomTo) {
        if (mMap != null) {
            mMap.setMyLocationStyle(setLocationStyle());
            mMap.setMyLocationEnabled(true);// 设置为true表示启动显示定位蓝点，false表示隐藏定位蓝点并不进行定位，默认是false。
            mMap.moveCamera(CameraUpdateFactory.zoomTo(zoomTo));//缩放地图到指定的缩放级别,一共分为 17 级，从 3 到 19。数字越大，展示的图面信息越精细。
            UiSettings mUiSettings = mMap.getUiSettings();
            if (mUiSettings != null) {
                mUiSettings.setZoomControlsEnabled(false); //关闭缩放按钮
                mUiSettings.setMyLocationButtonEnabled(false); //关闭定位按钮，界面额外添加
            }
            showLoading();
            LocationUtils.getInstance()
                    .setOnLocationListener(this)
                    .setOnLocationErrorListener(new LocationUtils.OnLocationErrorListener() {
                        @Override
                        public void onLocationErrorListener(AMapLocation aMapLocation) {
                            hideLoading();
                            if (aMapLocation.getErrorCode() == 12) {
                                ToastUtils.showShort("请打开GPS定位");
                            } else {
                                ToastUtils.showShort("定位失败");
                            }
                        }
                    })
                    .startLocation();
        }
    }

    @Override
    public void onLocationListener(AMapLocation aMapLocation, String address, double latitude, double longitude) {
        hideLoading();
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), 16));
    }

    /**
     * 设置接口之前保证隐私政策合规
     */
    protected void update() {
        //在构造AMapLocationClient 之前必须进行合规检查，设置接口之前保证隐私政策合规
        MapManager.updatePrivacy(new IMapChecker() {
            @Override
            public void updatePrivacyShow(boolean isContains, boolean isShow) {
                MapsInitializer.updatePrivacyShow(getApplicationContext(), isContains, isShow);
                AMapLocationClient.updatePrivacyShow(getApplicationContext(), isContains, isShow);
            }

            @Override
            public void updatePrivacyAgree(boolean isAgree) {
                MapsInitializer.updatePrivacyAgree(getApplicationContext(), isAgree);
                AMapLocationClient.updatePrivacyAgree(getApplicationContext(), isAgree);
            }
        });
    }

    /**
     * 设置定位回调监听
     */
    protected MyLocationStyle setLocationStyle() {
        //定位
        MyLocationStyle myLocationStyle = new MyLocationStyle();
        //只定位一次
        myLocationStyle.myLocationType(MyLocationStyle.LOCATION_TYPE_SHOW);
        return myLocationStyle;
    }

    //**************************************** 定位 ***********************************************//

    /**
     * @param latLng:定位点，默认定位等级16
     */
    protected void location(LatLng latLng) {
        location(latLng, 16);
    }

    /**
     * @param latLng:定位点
     * @param zoomTo：:缩放地图到指定的缩放级别,一共分为 17 级，从 3 到 19。数字越大，展示的图面信息越精细。
     */
    protected void location(LatLng latLng, float zoomTo) {
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoomTo));
    }

    //**************************************** 绘制点、线、面 ***************************************//

    /**
     * @param latLngArray：绘制一段线条
     * @param properties：绘制属性
     */
    protected Polyline drawPolyline(List<LatLng> latLngArray, DrawPolylineProperties properties) {
        if (mMap != null) {
            return mMap.addPolyline(new PolylineOptions().
                    addAll(latLngArray)
                    .width(properties.getWidth())
                    .color(properties.getColor()));
        }
        return null;
    }

    /**
     * @param latLngArray:绘制一个多边形
     */
    protected Polygon drawPolygon(List<LatLng> latLngArray) {
        final DrawPolygonProperties options = new DrawPolygonProperties(
                Color.parseColor("#30ff0000"),
                Color.parseColor("#30D0E6F3"),
                1);
        return drawPolygon(latLngArray, options);
    }

    /**
     * @param latLngArray:绘制一个多边形
     * @param properties:绘制属性
     */
    protected Polygon drawPolygon(List<LatLng> latLngArray, DrawPolygonProperties properties) {
        if (mMap != null) {
            return drawPolygon(new PolygonOptions()
                    .addAll(latLngArray)
                    .fillColor(properties.getFillColor())
                    .strokeColor(properties.getStrokeColor())
                    .strokeWidth(properties.getStrokeWidth()));
        }
        return null;
    }

    /**
     * @param polygon:绘制一个多边形
     */
    protected Polygon drawPolygon(PolygonOptions polygon) {
        if (mMap != null) {
            return mMap.addPolygon(polygon);
        }
        return null;
    }

    /**
     * @param latLng:绘制一个点
     */
    protected Marker drawMarker(LatLng latLng) {
        return drawMarker(latLng, R.drawable.map_location_marker);
    }


    /**
     * @param latLng:绘制一个点
     * @param icon:绘制一个点的图片
     */
    protected Marker drawMarker(LatLng latLng, int icon) {
        if (mMap != null) {
            BitmapDescriptor bitmapDescriptor = BitmapDescriptorFactory.fromBitmap(BitmapFactory
                    .decodeResource(getResources(), icon));
            return mMap.addMarker(new MarkerOptions()
                    .position(latLng)
                    .icon(bitmapDescriptor));
        }
        return null;
    }

    /**
     * @param listener:绘制点点击事件
     */
    protected void setOnMarkerClickListener(AMap.OnMarkerClickListener listener) {
        if (mMap != null) {
            mMap.setOnMarkerClickListener(listener);
        }
    }

    //**************************************** 路径规划 *********************************//

    /**
     * 驾车路线规划
     *
     * @param mStartPoint:起点
     * @param mEndPoint:终点
     */
    protected void setPlanningDrive(LatLng mStartPoint, LatLng mEndPoint) {
        if (mStartPoint == null) {
            ToastUtils.showShort("起点未设置");
            return;
        }
        if (mEndPoint == null) {
            ToastUtils.showShort("终点未设置");
            return;
        }
        setPlanningDrive(AMapUtil.convertToLatLonPoint(mStartPoint), AMapUtil.convertToLatLonPoint(mEndPoint));
    }

    /**
     * 驾车路线规划
     *
     * @param mStartPoint:起点
     * @param mEndPoint:终点
     */
    protected void setPlanningDrive(LatLonPoint mStartPoint, LatLonPoint mEndPoint) {
        if (mStartPoint == null) {
            ToastUtils.showShort("起点未设置");
            return;
        }
        if (mEndPoint == null) {
            ToastUtils.showShort("终点未设置");
            return;
        }
        final RouteSearch.FromAndTo fromAndTo = new RouteSearch.FromAndTo(mStartPoint, mEndPoint);
        RouteSearch.DriveRouteQuery query = new RouteSearch.DriveRouteQuery(fromAndTo,
                RouteSearch.DRIVING_SINGLE_DEFAULT, null, null, "");
        if (mRouteSearch != null) {
            mRouteSearch.calculateDriveRouteAsyn(query);
        }
    }

    /**
     * 步行路线规划
     *
     * @param mStartPoint:起点
     * @param mEndPoint:终点
     */
    protected void setPlanningWalk(LatLng mStartPoint, LatLng mEndPoint) {
        if (mStartPoint == null) {
            ToastUtils.showShort("起点未设置");
            return;
        }
        if (mEndPoint == null) {
            ToastUtils.showShort("终点未设置");
            return;
        }
        setPlanningWalk(AMapUtil.convertToLatLonPoint(mStartPoint), AMapUtil.convertToLatLonPoint(mEndPoint));
    }

    /**
     * 步行路线规划
     *
     * @param mStartPoint:起点
     * @param mEndPoint:终点
     */
    protected void setPlanningWalk(LatLonPoint mStartPoint, LatLonPoint mEndPoint) {
        if (mStartPoint == null) {
            ToastUtils.showShort("起点未设置");
            return;
        }
        if (mEndPoint == null) {
            ToastUtils.showShort("终点未设置");
            return;
        }
        final RouteSearch.FromAndTo fromAndTo = new RouteSearch.FromAndTo(mStartPoint, mEndPoint);
        RouteSearch.WalkRouteQuery query = new RouteSearch.WalkRouteQuery(fromAndTo);
        if (mRouteSearch != null) {
            mRouteSearch.calculateWalkRouteAsyn(query);
        }
    }

    //**************************************** 坐标点转换为高德坐标系 *********************************//

    /***
     * （GPS）坐标点转换为高德坐标系
     * @param latLng:待转换坐标点 LatLng类型
     */
    protected LatLng latLngToGd(LatLng latLng) {
        return latLngToGd(latLng, CoordinateConverter.CoordType.GPS);
    }

    /***
     * 坐标点转换为高德坐标系
     * @param latLng:待转换坐标点 LatLng类型
     * @param var:待转换坐标类型
     */
    protected LatLng latLngToGd(LatLng latLng, CoordinateConverter.CoordType var) {
        CoordinateConverter converter = new CoordinateConverter(this);
        // sourceLatLng待转换坐标点 LatLng类型
        converter.coord(latLng);
        // CoordType.GPS 待转换坐标类型
        converter.from(var);
        // 执行转换操作
        return converter.convert();
    }

    @Override
    public void onBusRouteSearched(BusRouteResult busRouteResult, int i) {

    }

    @Override
    public void onDriveRouteSearched(DriveRouteResult result, int errorCode) {
        if (mMap != null) {
            // 清理地图上的所有覆盖物
            mMap.clear();
            if (errorCode == AMapException.CODE_AMAP_SUCCESS) {
                if (result != null && result.getPaths() != null && result.getPaths().size() > 0) {
                    // 获取路径规划结果对象
                    final DrivePath drivePath = result.getPaths().get(0);
                    if (drivePath == null) {
                        return;
                    }
                    // 绘制路线
                    DrivingRouteOverlay overlay = new DrivingRouteOverlay(this,
                            mMap, drivePath,
                            result.getStartPos(),
                            result.getTargetPos());
                    // 显示路径规划结果
                    overlay.setNodeIconVisibility(false);//设置节点marker是否显示
                    overlay.setIsColorFull(true);//是否用颜色展示交通拥堵情况，默认true
                    overlay.removeFromMap();
                    overlay.addToMap();
                    overlay.zoomToSpan();

                } else {
                    ToastUtils.showShort("对不起，没有搜索到相关数据！");
                }
            } else {
                // 搜索失败，获取错误码和错误信息
                LatteLogger.e("错误码：" + errorCode);
            }
        }
    }


    /**
     * 步行
     */
    @Override
    public void onWalkRouteSearched(WalkRouteResult result, int errorCode) {

    }

    /**
     * 公交换乘
     */
    @Override
    public void onRideRouteSearched(RideRouteResult result, int errorCode) {

    }


}
