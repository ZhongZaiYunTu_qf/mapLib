package com.zz.yt.lib.map.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.amap.api.location.AMapLocation;
import com.blankj.utilcode.util.GsonUtils;
import com.blankj.utilcode.util.JsonUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.whf.android.jar.net.RestClient;
import com.whf.android.jar.net.callback.ISuccess;
import com.whf.android.jar.util.log.LatteLogger;
import com.zz.yt.lib.map.R;
import com.zz.yt.lib.map.bean.WeatherBean;
import com.zz.yt.lib.map.utils.LocationUtils;
import com.zz.yt.lib.map.utils.WeatherIconUtils;

import java.util.List;


/**
 * 天气
 *
 * @author gc
 * @version 1.0
 */
public class WeatherView extends LinearLayout {

    private final TextView tvTemp;
    private final TextView tvWeather;
    private final ImageView ivWeatherIcon;


    public WeatherView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater.from(context).inflate(R.layout.hai_map_view_weather, this, true);

        tvTemp = findViewById(R.id.tv_temp);
        tvWeather = findViewById(R.id.tv_weather);
        ivWeatherIcon = findViewById(R.id.iv_weatherIcon);

        TypedArray attributes = context.obtainStyledAttributes(attrs, R.styleable.WeatherView);

        final Drawable background = attributes.getDrawable(R.styleable.WeatherView_android_background);
        if (background != null) {
            setBackground(background);
        }
        //textView
        String titleText = attributes.getString(R.styleable.WeatherView_android_text);
        if (!TextUtils.isEmpty(titleText)) {
            tvTemp.setText(titleText);
            tvWeather.setText(titleText);
        }
        int titleTextColor = attributes.getColor(R.styleable.WeatherView_android_textColor, -1);
        if (titleTextColor != -1) {
            tvTemp.setTextColor(titleTextColor);
            tvWeather.setTextColor(titleTextColor);
        }
        int titleTextSize = attributes.getInt(R.styleable.WeatherView_android_textSize, -1);
        if (titleTextSize != -1) {
            tvTemp.setTextSize(titleTextSize);
            tvWeather.setTextSize(titleTextSize);
        }
        attributes.recycle();

        init();
    }

    private void init() {
        //初始化定位
        LocationUtils.getInstance()
                .setOnLocationListener(new LocationUtils.OnLocationListener() {
                    @Override
                    public void onLocationListener(AMapLocation aMapLocation, String address, double latitude, double longitude) {
                        //定位成功回调信息，设置相关消息
                        LocationUtils.getInstance().stopLocation();
                        RestClient.builder()
                                .url("https://restapi.amap.com/v3/weather/weatherInfo")
                                .params("key", "df36846abd617afe510ac1f2be398b48")
                                .params("city", aMapLocation.getAdCode())
                                .params("extensions", "all")//此参数改动后会导致返回数据类型不一致
                                .success(new ISuccess() {
                                    @Override
                                    public void onSuccess(String response) {
                                        LatteLogger.json(response);
                                        if (1 == JsonUtils.getInt(response, "status")) {
                                            final String data = JsonUtils.getString(response, "forecasts");
                                            List<WeatherBean> beanList = GsonUtils.fromJson(data, GsonUtils.getListType(WeatherBean.class));
                                            WeatherBean weatherBean = beanList.get(0);
                                            if (weatherBean.getCasts().isEmpty()) {
                                                Log.e("GetWeatherError", "data is empty");
                                                return;
                                            }
                                            WeatherBean.CastsBean bean = weatherBean.getCasts().get(0);
                                            String temp = bean.getDaytemp() + "~" + bean.getNighttemp() + "℃";
                                            String weather = bean.getDayweather() + "转" + bean.getNightweather();
                                            if (bean.getDayweather().equals(bean.getNightweather())) {
                                                weather = bean.getDayweather();
                                            }
                                            tvTemp.setText(temp);
                                            tvWeather.setText(weather);
                                            ivWeatherIcon.setImageResource(WeatherIconUtils.getIcon(bean.getDayweather()));

                                        } else {
                                            String info = JsonUtils.getString(response, "info");
                                            String infocode = JsonUtils.getString(response, "infocode");
                                            LatteLogger.e("GetWeatherError", "infocode:" + infocode + ", info:" + info);
                                        }
                                    }
                                })
                                .build()
                                .get();
                    }
                })
                .startLocation();
        //
        LocationUtils.getInstance().setOnLocationErrorListener(new LocationUtils.OnLocationErrorListener() {
            @Override
            public void onLocationErrorListener(AMapLocation aMapLocation) {
                if (aMapLocation.getErrorCode() == 12) {
                    ToastUtils.showShort("请打开GPS定位");
                } else {
                    ToastUtils.showShort("定位失败");
                }
            }
        });
    }

}
