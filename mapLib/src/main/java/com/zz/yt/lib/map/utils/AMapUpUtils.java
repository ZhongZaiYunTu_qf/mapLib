package com.zz.yt.lib.map.utils;


import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.amap.api.location.AMapLocation;
import com.amap.api.location.AMapLocationClient;
import com.amap.api.maps.MapsInitializer;
import com.amap.api.maps.model.LatLng;
import com.blankj.utilcode.util.GsonUtils;
import com.blankj.utilcode.util.TimeUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.whf.android.jar.app.Latte;
import com.whf.android.jar.constants.UserConstant;
import com.whf.android.jar.net.RestClient;
import com.whf.android.jar.net.callback.ISuccess;
import com.whf.android.jar.util.callback.CallbackManager;
import com.whf.android.jar.util.callback.IGlobalCallback;
import com.whf.android.jar.util.log.LatteLogger;
import com.whf.android.jar.util.storage.LattePreference;
import com.zz.yt.lib.map.checker.IMapChecker;
import com.zz.yt.lib.map.checker.MapManager;


import java.util.WeakHashMap;


/***
 * 上传Gps
 *
 * @author qf
 * @author 中再云图技术有限公司
 * @version 1.0
 */
public class AMapUpUtils {

    private volatile static AMapUpUtils mInstance;
    private static final String GPS = "gps";
    private final int mInterval;
    private String url;

    @NonNull
    public static AMapUpUtils getInstance() {
        return getInstance(30 * 1000);
    }

    @NonNull
    public static AMapUpUtils getInstance(int interval) {
        if (mInstance == null) {
            synchronized (AMapUpUtils.class) {
                if (mInstance == null) {
                    mInstance = new AMapUpUtils(interval);
                }
            }
        }
        return mInstance;
    }

    public AMapUpUtils(int mInterval) {
        this.mInterval = mInterval;
        //在构造AMapLocationClient 之前必须进行合规检查，设置接口之前保证隐私政策合规
        MapManager.updatePrivacy(new IMapChecker() {
            @Override
            public void updatePrivacyShow(boolean isContains, boolean isShow) {
                MapsInitializer.updatePrivacyShow(Latte.getApplicationContext(), isContains, isShow);
                AMapLocationClient.updatePrivacyShow(Latte.getApplicationContext(), isContains, isShow);
            }

            @Override
            public void updatePrivacyAgree(boolean isAgree) {
                MapsInitializer.updatePrivacyAgree(Latte.getApplicationContext(), isAgree);
                AMapLocationClient.updatePrivacyAgree(Latte.getApplicationContext(), isAgree);
            }
        });
    }

    /**
     * 设置定位权限
     */
    public void setPermissions() {
        @SuppressWarnings("unchecked")
        IGlobalCallback<Object> callback = CallbackManager
                .getInstance()
                .getCallback(GPS);
        callback.executeCallback(null);
    }

    /**
     * @param url：设置网址
     */
    public AMapUpUtils setUrl(String url) {
        this.url = url;
        //获得定位权限后上传gps
        CallbackManager.getInstance().addCallback(GPS, new IGlobalCallback() {
            @Override
            public void executeCallback(@Nullable Object args) {
                setSignToken();
            }
        });
        return mInstance;
    }

    /**
     * 上传GPS
     */
    private void setSignToken() {
        LocationUtils.getInstance().setDestroy(mInterval).setOnLocationListener(new LocationUtils.OnLocationListener() {
            @Override
            public void onLocationListener(AMapLocation aMapLocation, String address, double latitude, double longitude) {
                //存储经纬度
                setLatLng(latitude, longitude);
                //定位成功回调信息，设置相关消息
                WeakHashMap<String, Object> params = new WeakHashMap<>();
                params.put("punchTime", TimeUtils.getNowString());
                params.put("address", address);//获取位置
                params.put("country", aMapLocation.getCountry());
                params.put("province", aMapLocation.getProvince());
                params.put("city", aMapLocation.getCity());
                params.put("district", aMapLocation.getDistrict());
                params.put("street", aMapLocation.getStreet());
                params.put("streetNum", aMapLocation.getStreetNum());
                params.put("latitude", latitude);//获取纬度
                params.put("longitude", longitude);//获取经度
                params.put("userId", LattePreference.getCustomAppProfile(UserConstant.USER_ID));
                if (TextUtils.isEmpty(url)) {
                    LatteLogger.json(GsonUtils.toJson(params));
                } else {
                    RestClient.builder()
                            .url(url)
                            .raw(GsonUtils.toJson(params))
                            .success(new ISuccess() {
                                @Override
                                public void onSuccess(String response) {
                                    LatteLogger.json(response);
                                }
                            })
                            .build()
                            .post();
                }
            }
        });

        LocationUtils.getInstance().setOnLocationErrorListener(new LocationUtils.OnLocationErrorListener() {
            @Override
            public void onLocationErrorListener(AMapLocation aMapLocation) {
                if (aMapLocation.getErrorCode() == 12) {
                    ToastUtils.showShort("请打开GPS定位");
                } else {
                    ToastUtils.showShort("定位失败");
                }
            }
        });
    }


    /**
     * @param latitude:存储经纬度
     * @param longitude:存储经纬度
     */
    private void setLatLng(double latitude, double longitude) {
        LattePreference.addCustomAppProfile("lat", latitude + "");
        LattePreference.addCustomAppProfile("lng", longitude + "");
    }

    /**
     * 获得定位点
     */
    @NonNull
    public static LatLng latLng() {
        String latitude = LattePreference.getCustomAppProfile("lat", "0.0");
        String longitude = LattePreference.getCustomAppProfile("lng", "0.0");
        double lat = Double.parseDouble(latitude);
        double lng = Double.parseDouble(longitude);
        return new LatLng(lat, lng);
    }
}
