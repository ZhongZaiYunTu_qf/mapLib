package com.zz.yt.lib.map.input;

import com.amap.api.services.help.Tip;

/**
 * 点击事件
 *
 * @author qf
 * @version 1.0
 **/
public interface OnClickTipListener {


    /**
     * @param tip：点击值
     */
    void onClickTip(Tip tip);
}
