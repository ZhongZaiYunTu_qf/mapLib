package com.zz.yt.lib.map.utils;

import com.amap.api.location.AMapLocation;
import com.whf.android.jar.dao.LocateDao;
import com.whf.android.jar.net.HttpCode;

/**
 * 保存上一次的定位信息
 * <p>需要继承 不能加final</>
 *
 * @author qf
 * @version 2023-04-23
 */
public final class AMapLocationUtils extends LocateDao {

    /**
     * <p>如果option中设置isNeedAddress为false，则没有此结果，网络定位结果中会有地址信息，GPS定位不返回地址信息。
     *
     * @param aMapLocation:高德定位信息储存
     */
    public static void onLocationChanged(AMapLocation aMapLocation) {
        if (aMapLocation != null && aMapLocation.getErrorCode() == HttpCode.CODE_0) {
            //可在其中解析aMapLocation获取相应内容。
            setAddress(aMapLocation.getAddress()); //地址
            setLongitude(aMapLocation.getLongitude() + "");
            setLatitude(aMapLocation.getLatitude() + "");
            setCountry(aMapLocation.getCountry());//国家信息
            setAccuracy(aMapLocation.getAccuracy() + "");
            setProvince(aMapLocation.getProvince());//省
            setCity(aMapLocation.getCity());//城市信息
            setDistrict(aMapLocation.getDistrict());//城区信息
            setStreet(aMapLocation.getStreet());//街道信息
            setStreetNum(aMapLocation.getStreetNum());//街道门牌号信息
            setCityCode(aMapLocation.getCityCode());
            setAdCode(aMapLocation.getAdCode());
            setAoiName(aMapLocation.getAoiName());
            setFloor(aMapLocation.getFloor());
        }
    }
}
