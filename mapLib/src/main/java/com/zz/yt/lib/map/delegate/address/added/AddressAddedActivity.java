package com.zz.yt.lib.map.delegate.address.added;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;


import com.zz.yt.lib.map.R;
import com.zz.yt.lib.map.delegate.address.entity.AddressEntity;
import com.zz.yt.lib.map.delegate.selection.entity.MapListEntity;
import com.zz.yt.lib.ui.BundleKey;
import com.zz.yt.lib.ui.CustomTitleBar;
import com.zz.yt.lib.ui.SwitchListLayout;
import com.zz.yt.lib.ui.base.ui.LatteTitleActivity;
import com.zz.yt.lib.ui.item.EditLayoutBar;
import com.zz.yt.lib.ui.listener.OnClickStringListener;


/***
 * 新建收货地址
 * @author qf
 * @version 1.0
 */
public class AddressAddedActivity extends LatteTitleActivity {

    private EditLayoutBar editConsignee;
    private EditLayoutBar editTelephone;
    private EditLayoutBar editRegion;
    private EditLayoutBar editAddress;

    private boolean isChecked = true;
    private AddressEntity entity;

    private void getData() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            entity = (AddressEntity) bundle.getSerializable(BundleKey.ENTITY.name());
        }
    }

    @Override
    public void onBindView(Bundle savedInstanceState) {
        Button button = findViewById(R.id.id_btn_submit);
        if (button != null) {
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onClickSubmit();
                }
            });
        }
        getData();
        editConsignee = findViewById(R.id.id_edit_consignee);
        editTelephone = findViewById(R.id.id_edit_telephone);
        editRegion = findViewById(R.id.id_edit_region);
        editAddress = findViewById(R.id.id_edit_address);
        SwitchListLayout sllDefault = findViewById(R.id.id_sll_default);

        if (editRegion != null) {
            AddressPopupUtils.create(this, getWindow().getDecorView(), editRegion).setOnClickListener(new OnClickStringListener() {
                @Override
                public void onClick(String index) {
                    if (editAddress != null) {
                        editAddress.setText("");
                    }
                }
            });
        }
        if (entity != null) {
            editConsignee.setText(entity.getConsignee());
            editTelephone.setText(entity.getTelephone());
            editRegion.setText(entity.getRegion());
            editAddress.setText(entity.getAddress());

            if (sllDefault != null) {
                sllDefault.getSwitchRightView().setChecked(entity.isDefault());
            }
        }
        if (sllDefault != null) {
            sllDefault.setSwitchRightCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean is) {
                    isChecked = is;
                }
            });
        }

    }

    @Override
    protected void setCustomTitleBar(CustomTitleBar titleBar) {
        if (titleBar != null) {
            titleBar.setText("新建地址");
        }
    }

    @Override
    protected Object setLayout() {
        return R.layout.hai_map_delegate_address_added;
    }

    void onClickSubmit() {
        AddressEntity entity = new AddressEntity();
        entity.setConsignee(editConsignee.toString());
        if (editConsignee.isString()) {
            return;
        }
        entity.setTelephone(editTelephone.toString());
        if (editTelephone.isString()) {
            return;
        }
        entity.setRegion(editRegion.toString());
        if (editRegion.isString()) {
            return;
        }
        entity.setAddress(editAddress.toString());
        if (editAddress.isString()) {
            return;
        }
        entity.setDefault(isChecked);

        Intent intent = new Intent();
        intent.putExtra(BundleKey.ENTITY.name(), entity);
        setResult(Activity.RESULT_OK, intent);
        finish();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == AddressPopupUtils.REQUEST_CODE && data != null) {
            MapListEntity mapListEntity = (MapListEntity) data.getExtras().getSerializable(BundleKey.ENTITY.name());
            if (mapListEntity != null) {
                editRegion.setText(mapListEntity.getRegion());
                editAddress.setText(mapListEntity.getAddress());
            }
        }
    }
}
