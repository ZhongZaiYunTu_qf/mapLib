package com.zz.yt.lib.map;

import com.amap.api.location.AMapLocationClient;
import com.amap.api.maps.MapsInitializer;
import com.whf.android.jar.app.Latte;
import com.zz.yt.lib.map.checker.IMapChecker;
import com.zz.yt.lib.map.checker.MapManager;

/**
 * 高德地图
 *
 * @author qf
 * @version 1.0.1
 */
public final class MapUtils {

    /**
     * 高德地图初始化
     */
    public static void initApp() {
        //高德地图
        MapManager.setMapContains(true, true);
        MapManager.setMapAgree(true);

        //在构造AMapLocationClient 之前必须进行合规检查，设置接口之前保证隐私政策合规
        MapManager.updatePrivacy(new IMapChecker() {
            @Override
            public void updatePrivacyShow(boolean isContains, boolean isShow) {
                MapsInitializer.updatePrivacyShow(Latte.getApplicationContext(), isContains, isShow);
                AMapLocationClient.updatePrivacyShow(Latte.getApplicationContext(), isContains, isShow);
            }

            @Override
            public void updatePrivacyAgree(boolean isAgree) {
                MapsInitializer.updatePrivacyAgree(Latte.getApplicationContext(), isAgree);
                AMapLocationClient.updatePrivacyAgree(Latte.getApplicationContext(), isAgree);
            }
        });

    }
}
