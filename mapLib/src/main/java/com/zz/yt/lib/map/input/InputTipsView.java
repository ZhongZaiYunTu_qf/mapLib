package com.zz.yt.lib.map.input;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.amap.api.services.help.Tip;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.zz.yt.lib.map.R;
import com.zz.yt.lib.mvp.base.view.BaseMvpRecyclerLayout;
import com.zz.yt.lib.ui.recycler.holder.MultipleViewHolder;

public class InputTipsView extends BaseMvpRecyclerLayout<Tip> {

    private OnClickTipListener mClickTipListener = null;

    public InputTipsView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected int setLayout() {
        return R.layout.a_ui_view_recycler;
    }

    public void setOnClickTipListener(OnClickTipListener listener) {
        this.mClickTipListener = listener;
    }

    @Override
    protected RecyclerView setRecyclerView() {
        return findViewById(R.id.rl_customer);
    }

    @Override
    public int setLayoutItem() {
        return R.layout.hai_map_item_input_tips;
    }

    @Override
    public void convert(@NonNull MultipleViewHolder holder, @NonNull Object entity, Object other) {
        Tip tip = dataItem.get(holder.position());
        holder.setText(R.id.title, tip.getName());
    }

    @Override
    public void onItemClick(@NonNull BaseQuickAdapter<?, ?> adapter, @NonNull View view, int position) {
        super.onItemClick(adapter, view, position);
        if (mClickTipListener != null) {
            mClickTipListener.onClickTip(dataItem.get(position));
        }
    }
}
