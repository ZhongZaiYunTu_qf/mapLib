package com.zz.yt.lib.map.delegate.selection;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.amap.api.location.AMapLocation;
import com.amap.api.maps.AMap;
import com.amap.api.maps.model.CameraPosition;
import com.amap.api.maps.model.LatLng;
import com.amap.api.maps.model.Marker;
import com.amap.api.maps.model.MarkerOptions;
import com.amap.api.services.core.AMapException;
import com.amap.api.services.core.LatLonPoint;
import com.amap.api.services.core.PoiItem;
import com.amap.api.services.poisearch.PoiResult;
import com.amap.api.services.poisearch.PoiSearch;
import com.blankj.utilcode.util.BarUtils;
import com.blankj.utilcode.util.ObjectUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.zz.yt.lib.map.delegate.selection.adapter.MapListAdapter;
import com.zz.yt.lib.map.delegate.selection.entity.MapListEntity;
import com.zz.yt.lib.map.R;
import com.zz.yt.lib.map.base.BaseMapFragmentActivity;
import com.zz.yt.lib.map.utils.LocationUtils;
import com.zz.yt.lib.ui.BundleKey;
import com.zz.yt.lib.ui.CustomTitleBar;

import java.util.ArrayList;
import java.util.List;

/**
 * 位置选择
 *
 * @author qf
 * @version 1.0
 */
public class LocationSelectionActivity extends BaseMapFragmentActivity {

    public static final String CITY_CODE = "CityCode";

    private RelativeLayout rlTitle;
    private RecyclerView recyclerView;
    private SearchView search;
    private ImageView close;
    private TextView finish;
    private ImageView location;

    private MapListAdapter adapter;
    private Marker marker;

    // >=2 时，认为页面加载完成
    private int isFinish = 0;

    private List<MapListEntity> list = new ArrayList<>();
    private String cityCode = "190000";

    @Override
    protected void setCustomTitleBar(CustomTitleBar titleBar) {
        if (titleBar != null) {
            titleBar.setText("选择位置");
        }
    }

    @Override
    protected Object setLayout() {
        return R.layout.hai_map_delegate_location_selection;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        showLoading();
    }

    @Override
    public void onBindView(Bundle savedInstanceState) {
        if (ObjectUtils.isNotEmpty(getIntent()) && ObjectUtils.isNotEmpty(getIntent().getExtras())) {
            cityCode = getIntent().getExtras().getString(CITY_CODE, "190000");
        }
        setUpMapIfNeeded();
        initView();
    }

    private void initView() {
        rlTitle = findViewById(R.id.rl_title);
        recyclerView = findViewById(R.id.recyclerView);
        search = findViewById(R.id.search);
        close = findViewById(R.id.close);
        finish = findViewById(R.id.finish);
        location = findViewById(R.id.location);

        initListener();
    }

    private void initListener() {
        BarUtils.transparentStatusBar(getWindow());
        BarUtils.setNavBarLightMode(getWindow(), true);
        addMarginTopEqualStatusBarHeight(rlTitle);

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MapListEntity mapBean = getSelect();
                if (ObjectUtils.isEmpty(mapBean)) {
                    ToastUtils.showShort("请选择地点");
                    return;
                }
                Intent intent = new Intent();
                intent.putExtra(BundleKey.ENTITY.name(), mapBean);
                setResult(Activity.RESULT_OK, intent);
                finish();
            }
        });

        //主动点击定位按钮
        location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LocationUtils.getInstance().startLocation();
                showLoading();
            }
        });

        setMapDate();

        //SearchView的搜索icon点击监听
        ImageView searchIcon = search.findViewById(R.id.search_mag_icon);
        final SearchView.SearchAutoComplete mSearchSrcTextView = search.findViewById(R.id.search_src_text);
        searchIcon.setClickable(true);
        searchIcon.setFocusable(true);
        searchIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //点击搜索图标后
                if (marker.isVisible()) {
                    CharSequence query = mSearchSrcTextView.getText();
                    query(query.toString(), cityCode, marker.getPosition().latitude, marker.getPosition().longitude);
                }
            }
        });

        search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                //点击搜索按钮后
                if (marker.isVisible()) {
                    query(query, cityCode, marker.getPosition().latitude, marker.getPosition().longitude);
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                //输入时内容变化
                return false;
            }
        });

        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        adapter = new MapListAdapter(new ArrayList<MapListEntity>());
        adapter.setOnItemClickListener(new MapListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                if (list.size() + 1 >= position) {
                    marker.remove();
                    setMarker(new LatLng(list.get(position).getLatitude(), list.get(position).getLongitude()));
                }
            }
        });
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

    }

    private void setMapDate() {
        initLocation(16);
        if (mMap != null) {
            mMap.setOnCameraChangeListener(new AMap.OnCameraChangeListener() {
                @Override
                public void onCameraChange(CameraPosition cameraPosition) {
                    marker.remove();
                }

                @Override
                public void onCameraChangeFinish(CameraPosition cameraPosition) {
                    isFinish++;
//                    hideLoading();
                    setMarker(cameraPosition.target);
                    query("", cityCode, cameraPosition.target.latitude, cameraPosition.target.longitude);
                }
            });

        }
    }


    //设置marker
    private void setMarker(LatLng latLng) {
        MarkerOptions markerOption = new MarkerOptions();
        markerOption.position(latLng);
        markerOption.draggable(true);
        marker = mMap.addMarker(markerOption);
    }

    /***
     * @param keyWord 表示搜索字符串
     * @param cityCode 表示POI搜索区域
     */
    private void query(String keyWord, String cityCode, double latitude, double longitude) {
        if (isFinish > 1) {
            showLoading();
        }
        list.clear();
        adapter.setList(list);
        PoiSearch.Query query = new PoiSearch.Query(keyWord, "", cityCode);
        // 设置每页最多返回多少条poiitem
        query.setPageSize(10);
        //设置查询页码
        query.setPageNum(1);
        PoiSearch poiSearch = null;
        try {
            poiSearch = new PoiSearch(this, query);
        } catch (AMapException e) {
            e.printStackTrace();
        }
        //构造 PoiSearch 对象，并设置监听
        if (poiSearch != null) {
            poiSearch.searchPOIAsyn();
            poiSearch.setBound(new PoiSearch.SearchBound(new LatLonPoint(latitude, longitude), 1000));
            poiSearch.setOnPoiSearchListener(new PoiSearch.OnPoiSearchListener() {
                @Override
                public void onPoiSearched(PoiResult poiResult, int i) {
                    if (isFinish > 1) {
                        hideLoading();
                    }
                    if (poiResult != null && i == 1000) {

                        for (PoiItem pois : poiResult.getPois()) {
                            LatLonPoint latLonPoint = pois.getLatLonPoint();
                            String region = pois.getProvinceName()
                                    + "-" + pois.getProvinceName()
                                    + "-" + pois.getProvinceName();
                            list.add(new MapListEntity(pois.getTitle(),
                                    pois.getDistance() + "",
                                    region,
                                    pois.getSnippet(),
                                    latLonPoint.getLatitude(),
                                    latLonPoint.getLongitude()));
                        }
                        adapter.setList(list);
                    }
                }

                @Override
                public void onPoiItemSearched(PoiItem poiItem, int i) {
                }
            });
        }
    }


    @Nullable
    private MapListEntity getSelect() {
        if (ObjectUtils.isEmpty(adapter)) {
            return null;
        }

        list = adapter.getData();
        for (MapListEntity bean : list) {
            if (bean.isSelect()) {
                return bean;
            }
        }

        return null;
    }

    @Override
    public void onLocationListener(AMapLocation aMapLocation, String address, double latitude, double longitude) {
        super.onLocationListener(aMapLocation, address, latitude, longitude);
//        hideLoading();
        marker.remove();
        query("", cityCode, latitude, longitude);
//        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), 16));
    }
}
