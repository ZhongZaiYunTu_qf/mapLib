package com.zz.yt.lib.map.delegate.weather;

import android.os.Bundle;

import androidx.annotation.Nullable;

import com.whf.android.jar.base.latte.BaseActivity;
import com.zz.yt.lib.map.R;

/**
 * 天气
 *
 * @author gc
 * @version 1.0
 */
public class WeatherActivity extends BaseActivity {
    @Override
    protected Object setLayout() {
        return R.layout.hai_map_delegate_weather;
    }

    @Override
    public void onBindView(@Nullable Bundle savedInstanceState) {

    }
}
