package com.zz.yt.lib.map.delegate.draw;


import android.app.Activity;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.widget.Button;

import androidx.annotation.Nullable;

import com.amap.api.maps.AMap;
import com.amap.api.maps.model.LatLng;
import com.blankj.utilcode.util.GsonUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.whf.android.jar.util.log.LatteLogger;
import com.zz.yt.lib.map.R;
import com.zz.yt.lib.map.base.BaseMapFragmentDelegate;
import com.zz.yt.lib.map.delegate.draw.properties.DrawPolygonProperties;
import com.zz.yt.lib.ui.BundleKey;
import com.zz.yt.lib.ui.CustomTitleBar;

import java.util.ArrayList;
import java.util.List;

/**
 * 开始绘制
 *
 * @author qf
 * @version 1.0
 */
public class MapFragmentDelegate extends BaseMapFragmentDelegate {

    private DrawPolygonProperties properties = null;
    private final List<LatLng> latLngArray = new ArrayList<>();

    @Override
    protected void setCustomTitleBar(CustomTitleBar titleBar) {
        if (titleBar != null) {
            titleBar.setText("开始绘制");
        }
    }

    @Override
    protected Object setLayout() {
        return R.layout.hai_map_delegate_base_map;
    }

    @Override
    protected void getData(Bundle bundle) {
        super.getData(bundle);
        properties = (DrawPolygonProperties) bundle.getSerializable(BundleKey.ENTITY.name());
    }

    @Override
    public void onLazyInitView(@Nullable Bundle savedInstanceState) {
        super.onLazyInitView(savedInstanceState);
        setUpMapIfNeeded();
        initView();
    }

    private void initView() {
        mMap.setOnMapClickListener(new AMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                drawMarker(latLng);
                latLngArray.add(latLng);
                if (properties == null) {
                    properties = new DrawPolygonProperties();
                }
                drawPolygon(latLngArray, properties);
                LatteLogger.i(GsonUtils.toJson(latLng));
            }
        });
        Button finish = findViewById(R.id.id_btn_submit);
        if (finish != null) {
            finish.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final int size = latLngArray.size();
                    Parcelable[] parcelables = new Parcelable[size];
                    if (size < 3) {
                        ToastUtils.showShort("请绘制图形");
                        return;
                    }
                    for (int i = 0; i < size; i++) {
                        parcelables[i] = latLngArray.get(i);
                    }
                    Bundle intent = new Bundle();
                    intent.putParcelableArray(BundleKey.ENTITY.name(), parcelables);
                    getSupportDelegate().setFragmentResult(Activity.RESULT_OK, intent);
                    getSupportDelegate().pop();
                }
            });
        }

        initLocation(16);
    }

    private void setArray(LatLng latLng) {
        if (properties == null) {
            properties = new DrawPolygonProperties();
        }
        latLngArray.add(latLng);

    }

}
