package com.zz.yt.lib.map.utils;

import com.zz.yt.lib.map.R;

public class WeatherIconUtils {

    public static int getIcon(String name) {
        int res;
        switch (name) {
            case "晴":
                res = R.mipmap.sun;
                break;
            case "少云":
                res = R.mipmap.yun1;
                break;
            case "晴间多云":
            case "多云":
                res = R.mipmap.yun2;
                break;
            case "阴":
                res = R.mipmap.yin;
                break;
            case "有风":
                res = R.mipmap.feng1;
                break;
            case "平静":
            case "微风":
            case "和风":
            case "清风":
                res = R.mipmap.feng2;
                break;
            case "强风/劲风":
            case "疾风":
            case "大风":
                res = R.mipmap.feng3;
                break;
            case "烈风":
            case "风暴":
            case "狂爆风":
            case "飓风":
            case "热带风暴":
            case "龙卷风":
                res = R.mipmap.feng4;
                break;
            case "霾":
                res = R.mipmap.mai1;
                break;
            case "中度霾":
                res = R.mipmap.mai2;
                break;
            case "重度霾":
                res = R.mipmap.mai3;
                break;
            case "严重霾":
                res = R.mipmap.mai4;
                break;
            case "阵雨":
                res = R.mipmap.yu1;
                break;
            case "雷阵雨":
                res = R.mipmap.yu2;
                break;
            case "雷阵雨并伴有冰雹":
                res = R.mipmap.yu3;
                break;
            case "小雨":
            case "小雨-中雨":
            case "毛毛雨/细雨":
                res = R.mipmap.yu4;
                break;
            case "中雨":
            case "中雨-大雨":
            case "雨":
                res = R.mipmap.yu5;
                break;
            case "大雨":
            case "大雨-暴雨":
                res = R.mipmap.yu6;
                break;
            case "暴雨":
            case "大暴雨-特大暴雨":
                res = R.mipmap.yu7;
                break;
            case "大暴雨":
                res = R.mipmap.yu8;
                break;
            case "特大暴雨":
            case "极端降雨":
                res = R.mipmap.yu9;
                break;
            case "强阵雨":
                res = R.mipmap.yu10;
                break;
            case "强雷阵雨":
                res = R.mipmap.yu11;
                break;
            case "雨雪天气":
                res = R.mipmap.yu12;
                break;
            case "雨夹雪":
                res = R.mipmap.yu13;
                break;
            case "阵雨夹雪":
                res = R.mipmap.yu14;
                break;
            case "冻雨":
                res = R.mipmap.yu15;
                break;
            case "雪":
            case "小雪":
            case "小雪-中雪":
                res = R.mipmap.xue1;
                break;
            case "阵雪":
                res = R.mipmap.xue2;
                break;
            case "中雪":
            case "中雪-大雪":
                res = R.mipmap.xue3;
                break;
            case "大雪":
            case "大雪-暴雪":
                res = R.mipmap.xue4;
                break;
            case "暴雪":
                res = R.mipmap.xue5;
                break;
            case "浮尘":
                res = R.mipmap.chen1;
                break;
            case "扬沙":
                res = R.mipmap.chen2;
                break;
            case "沙尘暴":
                res = R.mipmap.chen3;
                break;
            case "强沙尘暴":
                res = R.mipmap.chen4;
                break;
            case "雾":
                res = R.mipmap.wu1;
                break;
            case "浓雾":
                res = R.mipmap.wu2;
                break;
            case "强浓雾":
                res = R.mipmap.wu3;
                break;
            case "轻雾":
                res = R.mipmap.wu4;
                break;
            case "大雾":
                res = R.mipmap.wu5;
                break;
            case "特强浓雾":
                res = R.mipmap.wu6;
                break;
            case "热":
                res = R.mipmap.re;
                break;
            case "冷":
                res = R.mipmap.leng;
                break;
            default:
                res = R.mipmap.other;
                break;
        }
        return res;
    }
}
