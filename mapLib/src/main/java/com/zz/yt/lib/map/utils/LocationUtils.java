package com.zz.yt.lib.map.utils;

import android.os.Build;


import com.amap.api.location.AMapLocation;
import com.amap.api.location.AMapLocationClient;
import com.amap.api.location.AMapLocationClientOption;
import com.amap.api.location.AMapLocationListener;
import com.whf.android.jar.app.Latte;
import com.whf.android.jar.util.log.LatteLogger;


/***
 * 高德定位工具
 *
 * @author qf
 * @author 中再云图技术有限公司
 * @version 1.0
 */
public class LocationUtils {


    //1、保存唯一实例
    private volatile static LocationUtils mInstance;


    //声明AMapLocationClient类对象
    private AMapLocationClient mLocationClient = null;
    //声明AMapLocationClientOption对象
    private AMapLocationClientOption mLocationOption = null;
    private OnLocationListener onLocationListener;
    private OnLocationErrorListener onLocationErrorListener;

    //2、屏蔽外部的new
    private LocationUtils() {
        getLocation();
    }

    //3、提供一个全局访问点
    public static LocationUtils getInstance() {
        if (mInstance == null) {
            synchronized (LocationUtils.class) {
                if (mInstance == null) {
                    mInstance = new LocationUtils();
                }
            }
        }
        return mInstance;
    }

    public LocationUtils setDestroy(int interval) {
        mLocationOption.setOnceLocation(false);
        //mLocationOption.setOnceLocationLatest(false);
        mLocationOption.setInterval(interval);
        //设置场景模式后最好调用一次stop，再调用start以保证场景模式生效
        startLocation();
        return mInstance;
    }

    public LocationUtils setOnLocationListener(OnLocationListener onLocationListener) {
        this.onLocationListener = onLocationListener;
        return mInstance;
    }

    public LocationUtils setOnLocationErrorListener(OnLocationErrorListener onLocationErrorListener) {
        this.onLocationErrorListener = onLocationErrorListener;
        return mInstance;
    }

    /**
     * 启动定位
     */
    public void startLocation() {
        stopLocation();
        if (mLocationClient != null) {
            mLocationClient.startLocation();
        }
    }

    /**
     * 关闭定位
     */
    public void stopLocation() {
        if (mLocationClient != null) {
            mLocationClient.stopLocation();
        }
    }

    /**
     * 不再定位为了
     */
    public void onDestroy() {
        if (mLocationClient != null) {
            mLocationClient.stopLocation();//停止定位后，本地定位服务并不会被销毁
            mLocationClient.onDestroy();//销毁定位客户端，同时销毁本地定位服务。
        }
    }

    //声明定位回调监听器
    public AMapLocationListener mLocationListener = new AMapLocationListener() {
        @Override
        public void onLocationChanged(AMapLocation aMapLocation) {
            if (aMapLocation != null) {
                if (aMapLocation.getErrorCode() == 0) {
                    //可在其中解析amapLocation获取相应内容。
                    AMapLocationUtils.onLocationChanged(aMapLocation);
                    String address = aMapLocation.getAddress();//地址，如果option中设置isNeedAddress为false，则没有此结果，网络定位结果中会有地址信息，GPS定位不返回地址信息。

                    LatteLogger.d("address:------" + address);

                    if (onLocationListener != null) {
                        onLocationListener.onLocationListener(aMapLocation, address, aMapLocation.getLatitude(), aMapLocation.getLongitude());
                    }
                } else {
                    //定位失败时，可通过ErrCode（错误码）信息来确定失败的原因，errInfo是错误信息，详见错误码表。
                    LatteLogger.e("location Error, ErrCode:"
                            + aMapLocation.getErrorCode() + ", errInfo:"
                            + aMapLocation.getErrorInfo());
                    if (onLocationErrorListener != null) {
                        onLocationErrorListener.onLocationErrorListener(aMapLocation);
                    }
                }
            }
        }
    };

    public void getLocation() {
        //初始化定位
        try {
            mLocationClient = new AMapLocationClient(Latte.getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }
        //设置定位回调监听
        mLocationClient.setLocationListener(mLocationListener);
        //初始化AMapLocationClientOption对象
        mLocationOption = new AMapLocationClientOption();
        //设置定位模式为AMapLocationMode.Hight_Accuracy，高精度模式。
        mLocationOption.setLocationMode(AMapLocationClientOption.AMapLocationMode.Hight_Accuracy);
        //设置定位模式为AMapLocationMode.Battery_Saving，低功耗模式。
        //        mLocationOption.setLocationMode(AMapLocationClientOption.AMapLocationMode.Battery_Saving);
        //设置定位模式为AMapLocationMode.Device_Sensors，仅设备模式。
        //        mLocationOption.setLocationMode(AMapLocationClientOption.AMapLocationMode.Device_Sensors);

        //获取最近3s内精度最高的一次定位结果：
        //设置setOnceLocationLatest(boolean b)接口为true，启动定位时SDK会返回最近3s内精度最高的一次定位结果。
        // 如果设置其为true，setOnceLocation(boolean b)接口也会被设置为true，反之不会，默认为false。
        mLocationOption.setOnceLocationLatest(true);
        //获取一次定位结果：
        mLocationOption.setOnceLocation(true);
        //设置是否返回地址信息（默认返回地址信息）
        mLocationOption.setNeedAddress(true);
        //给定位客户端对象设置定位参数
        mLocationClient.setLocationOption(mLocationOption);

        //开启后台定位功能
        try {
            //android R 自带了通知了
            mLocationClient.disableBackgroundLocation(true);
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
                //开启后台定位通知功能
                mLocationClient.enableBackgroundLocation(13, AMapUtil.notification());
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    /**
     * 关闭后台定位通知
     */
    public void disableNotification() {
        if (mLocationClient != null) {
            mLocationClient.disableBackgroundLocation(false);
        }
    }


    public AMapLocationClientOption getLocationOption() {
        return mLocationOption;
    }

    public interface OnLocationListener {
        void onLocationListener(AMapLocation aMapLocation, String address, double latitude, double longitude);
    }

    public interface OnLocationErrorListener {
        void onLocationErrorListener(AMapLocation aMapLocation);
    }

}
