package com.zz.yt.lib.map.input;


import android.annotation.SuppressLint;

import com.amap.api.services.help.Tip;
import com.whf.android.jar.app.Latte;
import com.whf.android.jar.popup.impl.LattePopupDrawerView;
import com.zz.yt.lib.map.R;

import java.util.List;

/**
 * 搜索弹框
 */
@SuppressLint("ViewConstructor")
public class InputTipsPopup extends LattePopupDrawerView {

    private final List<Tip> list;
    private final OnClickTipListener mClickTipListener;

    public InputTipsPopup(List<Tip> list, OnClickTipListener listener) {
        super(Latte.getActivity());
        this.list = list;
        this.mClickTipListener = listener;
    }

    @Override
    protected int setLayout() {
        return R.layout.hai_map_popup_input_tips;
    }

    @Override
    protected void initViews() {
        InputTipsView recyclerView = findViewById(R.id.id_input_tips_view);
        if (recyclerView != null) {
            recyclerView.setData(list);
            recyclerView.setOnClickTipListener(new OnClickTipListener() {
                @Override
                public void onClickTip(Tip tip) {
                    if (mClickTipListener != null) {
                        mClickTipListener.onClickTip(tip);
                    }
                }
            });
        }
    }
}
