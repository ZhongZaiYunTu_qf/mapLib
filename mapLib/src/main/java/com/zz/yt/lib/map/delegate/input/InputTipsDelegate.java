package com.zz.yt.lib.map.delegate.input;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;

import com.amap.api.maps.CameraUpdateFactory;
import com.amap.api.maps.model.LatLng;
import com.amap.api.services.core.LatLonPoint;
import com.amap.api.services.help.Inputtips;
import com.amap.api.services.help.InputtipsQuery;
import com.amap.api.services.help.Tip;
import com.blankj.utilcode.util.GsonUtils;
import com.lxj.xpopup.enums.PopupPosition;
import com.whf.android.jar.util.log.LatteLogger;
import com.zz.yt.lib.map.R;
import com.zz.yt.lib.map.base.BaseMapViewDelegate;
import com.zz.yt.lib.map.input.InputTipsPopup;
import com.zz.yt.lib.map.input.OnClickTipListener;
import com.zz.yt.lib.ui.CustomTitleBar;

import java.util.List;

/**
 * 输入文字定位
 *
 * @author qf
 * @version 1.0
 */
public class InputTipsDelegate extends BaseMapViewDelegate implements Inputtips.InputtipsListener {

    /**
     * null或者“”代表在全国进行检索，否则按照传入的city进行检索
     */
    private String city = "";
    private SearchView searchView;

    @NonNull
    public static InputTipsDelegate create() {
        Bundle args = new Bundle();

        InputTipsDelegate fragment = new InputTipsDelegate();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int setIdMap() {
        return R.id.map;
    }

    @Override
    protected void setCustomTitleBar(@NonNull CustomTitleBar titleBar) {
        titleBar.setText("输入文字定位");
    }

    @Override
    protected Object setLayout() {
        return R.layout.hai_map_delegate_input_tips;
    }

    @Override
    public void onBindView(@Nullable Bundle savedInstanceState, @NonNull View rootView) {
        super.onBindView(savedInstanceState, rootView);
        searchView = findViewById(R.id.id_map_search_input);
        if (searchView != null) {
            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    return false;
                }

                @Override
                public boolean onQueryTextChange(String newText) {
                    return inSearch(newText);
                }
            });
        }
    }


    /***
     * 进行搜索
     * @param query：
     */
    protected boolean inSearch(String query) {
        LatteLogger.i(query);
        //第二个参数传入null或者“”代表在全国进行检索，否则按照传入的city进行检索
        InputtipsQuery inputQuery = new InputtipsQuery(query, city);
        inputQuery.setCityLimit(true);//限制在当前城市
        Inputtips inputTips = new Inputtips(context, inputQuery);
        inputTips.setInputtipsListener(this);
        inputTips.requestInputtipsAsyn();
        return true;
    }

    @Override
    public void onGetInputtips(List<Tip> list, int i) {
        LatteLogger.json(GsonUtils.toJson(list));
        if (list == null || list.size() == 0) {
            return;
        }
        new InputTipsPopup(list, new OnClickTipListener() {
            @Override
            public void onClickTip(Tip tip) {
                if (mMap != null) {
                    LatLonPoint point = tip.getPoint();
                    LatLng latLng = new LatLng(point.getLatitude(), point.getLongitude());
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 16));
                }
            }
        }).popup(searchView, PopupPosition.Bottom);
    }
}
