package com.zz.yt.lib.map.delegate.draw.properties;

import android.graphics.Color;

import java.io.Serializable;

/**
 * 多边形
 */
public class DrawPolygonProperties implements Serializable {

    /**
     * 填充颜色
     */
    private final int fillColor;
    /**
     * 描边颜色
     */
    private final int strokeColor;
    /**
     * 描边颜色宽度
     */
    private final int strokeWidth;

    public DrawPolygonProperties() {
        this.fillColor = Color.LTGRAY;
        this.strokeColor = Color.RED;
        this.strokeWidth = 1;
    }

    public DrawPolygonProperties(int fillColor, int strokeColor, int strokeWidth) {
        this.fillColor = fillColor;
        this.strokeColor = strokeColor;
        this.strokeWidth = strokeWidth;
    }

    public int getFillColor() {
        return fillColor;
    }

    public int getStrokeColor() {
        return strokeColor;
    }

    public int getStrokeWidth() {
        return strokeWidth;
    }
}
