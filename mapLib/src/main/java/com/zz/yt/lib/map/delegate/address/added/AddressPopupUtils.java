package com.zz.yt.lib.map.delegate.address.added;


import android.app.Activity;
import android.graphics.Color;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.bigkoo.pickerview.builder.OptionsPickerBuilder;
import com.bigkoo.pickerview.listener.OnOptionsSelectListener;
import com.bigkoo.pickerview.view.OptionsPickerView;
import com.blankj.utilcode.util.ActivityUtils;
import com.blankj.utilcode.util.GsonUtils;
import com.blankj.utilcode.util.ResourceUtils;
import com.whf.android.jar.app.Latte;
import com.whf.android.jar.util.log.LatteLogger;
import com.zz.yt.lib.map.delegate.selection.LocationSelectionActivity;
import com.zz.yt.lib.ui.item.EditLayoutBar;
import com.zz.yt.lib.ui.listener.OnClickStringListener;

import java.util.ArrayList;
import java.util.List;

/**
 * 城市选择
 *
 * @author qf
 * @version 1.0
 **/
public final class AddressPopupUtils implements View.OnClickListener {

    final static int REQUEST_CODE = 20210908;

    private final View ROOT_VIEW;
    private final EditLayoutBar mEditLayoutBar;
    private final List<String> options1Items = new ArrayList<>();
    private final List<List<String>> options2Items = new ArrayList<>();
    private final List<List<List<String>>> options3Items = new ArrayList<>();

    private OnClickStringListener mClickStringListener = null;

    @NonNull
    public static AddressPopupUtils create(Activity activity, View rootView, EditLayoutBar editLayoutBar) {
        return new AddressPopupUtils(activity, null, rootView, editLayoutBar);
    }

    @NonNull
    public static AddressPopupUtils create(Fragment fragment, View rootView, EditLayoutBar editLayoutBar) {
        return new AddressPopupUtils(null, fragment, rootView, editLayoutBar);
    }

    private AddressPopupUtils(final Activity activity,
                              final Fragment fragment,
                              @NonNull View rootView,
                              @NonNull EditLayoutBar editLayoutBar) {
        this.ROOT_VIEW = rootView;
        this.mEditLayoutBar = editLayoutBar;
        editLayoutBar.getValueView().setOnClickListener(this);
        editLayoutBar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LatteLogger.i("直接获取");
                if (activity != null) {
                    ActivityUtils.startActivityForResult(activity, LocationSelectionActivity.class, REQUEST_CODE);
                }
                if (fragment != null) {
                    ActivityUtils.startActivityForResult(fragment, LocationSelectionActivity.class, REQUEST_CODE);
                }
            }
        });
        initData();
    }

    private void initData() {
        final String assets = ResourceUtils.readAssets2String("province.json");
        final List<AddressBean> beanList = GsonUtils.fromJson(assets, GsonUtils.getListType(AddressBean.class));
        final int size = beanList == null ? 0 : beanList.size();
        //遍历省份
        for (int i = 0; i < size; i++) {
            final String name = beanList.get(i).getName();
            final List<AddressBean.CityBean> city = beanList.get(i).getCity();
            final int citySize = city == null ? 0 : city.size();
            //该省的城市列表（第二级）
            ArrayList<String> cityList = new ArrayList<>();
            //该省的所有地区列表（第三极）
            List<List<String>> province = new ArrayList<>();
            //遍历该省份的所有城市
            for (int j = 0; j < citySize; j++) {
                String cityName = city.get(j).getName();
                //添加城市
                cityList.add(cityName);
                //该城市的所有地区列表
                ArrayList<String> cityAreaList = new ArrayList<>(city.get(j).getArea());
                //添加该省所有地区数据
                province.add(cityAreaList);
            }

            /*
             * 添加城市数据
             */
            options1Items.add(name);
            /*
             * 添加城市数据
             */
            options2Items.add(cityList);

            /*
             * 添加地区数据
             */
            options3Items.add(province);
        }
    }

    public void setOnClickListener(OnClickStringListener listener) {
        this.mClickStringListener = listener;
    }

    @Override
    public void onClick(View v) {
        if (options1Items.size() == 0) {
            LatteLogger.e("assets文件夹没找到province.json文件");
            return;
        }
        OptionsPickerView<String> pvOptions = new OptionsPickerBuilder(Latte.getActivity(),
                new OnOptionsSelectListener() {
                    @Override
                    public void onOptionsSelect(int options1, int options2, int options3, View v) {
                        //返回的分别是三个级别的选中位置
                        String opt1tx = options1Items.size() > 0 ?
                                options1Items.get(options1) + "-" : "";

                        String opt2tx = options2Items.size() > 0
                                && options2Items.get(options1).size() > 0 ?
                                options2Items.get(options1).get(options2) + "-" : "";

                        String opt3tx = options2Items.size() > 0
                                && options3Items.get(options1).size() > 0
                                && options3Items.get(options1).get(options2).size() > 0 ?
                                options3Items.get(options1).get(options2).get(options3) : "";

                        String tx = opt1tx + opt2tx + opt3tx;

                        mEditLayoutBar.getValueView().setTag(tx);
                        mEditLayoutBar.getValueView().setText(tx);
                        if (mClickStringListener != null) {
                            mClickStringListener.onClick(tx);
                        }
                    }
                })
                .setTitleText("城市选择")
                .setDividerColor(Color.BLACK)
                //设置选中项文字颜色
                .setTextColorCenter(Color.BLACK)
                .setContentTextSize(14)
                .build();

        //三级选择器
        pvOptions.setPicker(options1Items, options2Items, options3Items);
        pvOptions.show(ROOT_VIEW);
    }


    static class AddressBean {

        private String name;
        private List<AddressBean.CityBean> city;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        private List<AddressBean.CityBean> getCity() {
            return city;
        }

        public void setCity(List<AddressBean.CityBean> city) {
            this.city = city;
        }

        public static class CityBean {

            private String name;
            private List<String> area;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            private List<String> getArea() {
                return area;
            }

            public void setArea(List<String> area) {
                this.area = area;
            }
        }
    }

}
