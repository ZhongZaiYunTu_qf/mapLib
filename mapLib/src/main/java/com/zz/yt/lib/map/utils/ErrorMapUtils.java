package com.zz.yt.lib.map.utils;

import androidx.annotation.Nullable;

import com.blankj.utilcode.util.ToastUtils;


/**
 * 导航的错误code
 */
public enum ErrorMapUtils {

    MAP_2(2, "请检查网络是否通畅，稍候再试。"),

    MAP_3(3,"起点错误，确保经纬度格式正常。"),

    MAP_4(4,"协议解析错误"),
    MAP_6(6,"终点错误，确保经纬度格式正常。"),
    MAP_7(7,"服务端编码异常"),
    MAP_8(8,"数据缺乏预览数据"),
    MAP_9(9,"数据格式错误"),
    MAP_10(10,"没有找到通向起点的道路"),
    MAP_11(11,"没有找到通向终点的道路"),
    MAP_12(12,"没有找到通向途经点的道路"),
    MAP_13(13,"用户key非法或过期"),
    MAP_17(17,"请求超出配额"),
    MAP_18(18,"请求参数非法"),
    MAP_19(19,"可能是由于连接的网络无法访问外网"),
    MAP_20(20,"起点/终点/途经点的距离太长"),
    MAP_21(21,"途经点错误"),
    MAP_22(22,"请检查签名包名与KEY绑定关系是否正确"),
    MAP_23(23,"单位时间内访问过于频繁"),
    MAP_24(24,"请求中使用的key与绑定平台不符"),
    MAP_25(25,"请检查起点是否正确，经纬度是否填反"),
    MAP_26(26,"请检查终点是否正确，经纬度是否填反"),
    MAP_28(28,"请检查调起组件的时候是否算路成功"),
    MAP_29(29,"路径规划与当前导航状态不匹配"),
    MAP_2999(2999,"有新的独立算路任务在进行中导致本次独立算路失败");


    // 成员变量
    private final String hint;
    private final int code;

    ErrorMapUtils(int code, String hint) {
        this.code = code;
        this.hint = hint;
    }

    /**
     * @param code:普通方法
     */
    @Nullable
    public static String getName(int code) {
        for (ErrorMapUtils c : ErrorMapUtils.values()) {
            if (c.code == code) {
                return c.hint;
            }
        }
        return null;
    }

    /**
     * @param code:提示方法
     */
    public static void showShort(int code) {
        ToastUtils.showShort(getName(code));
    }

}
