package com.zz.yt.lib.map.delegate.draw.properties;

import android.graphics.Color;

/**
 * 线条
 */
public class DrawPolylineProperties {

    /**
     * 线段的颜色
     */
    private final int COLOR;

    /**
     * 线段的宽度
     */
    private final int WIDTH;

    /**
     * 设置分段纹理list
     */

    private boolean isDottedLine;

    /**
     * 设置是否画虚线，默认为false，画实线。
     */

    private boolean useTexture;


    /**
     * 是否使用纹理贴图
     */
    private boolean useGradient;


    /**
     * 设置是否使用渐变色
     */
    private boolean isVisible;

    public DrawPolylineProperties() {
        this.COLOR = Color.RED;
        this.WIDTH = 10;
    }

    public DrawPolylineProperties(int strokeColor, int width) {
        this.COLOR = strokeColor;
        this.WIDTH = width;
    }


    public int getColor() {
        return COLOR;
    }

    public int getWidth() {
        return WIDTH;
    }

    public boolean isDottedLine() {
        return isDottedLine;
    }

    public void setDottedLine(boolean dottedLine) {
        isDottedLine = dottedLine;
    }

    public boolean isUseTexture() {
        return useTexture;
    }

    public void setUseTexture(boolean useTexture) {
        this.useTexture = useTexture;
    }

    public boolean isUseGradient() {
        return useGradient;
    }

    public void setUseGradient(boolean useGradient) {
        this.useGradient = useGradient;
    }

    public boolean isVisible() {
        return isVisible;
    }

    public void setVisible(boolean visible) {
        isVisible = visible;
    }
}
