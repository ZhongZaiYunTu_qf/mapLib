package com.zz.yt.lib.map.checker;

/**
 * 请求接口
 *
 * @author 傅令杰
 * @version  2017/4/22
 */

public interface IMapChecker {

    /**
     * @param isContains:是隐私权政策是否包含高德开平隐私权政策;rue是包含
     * @param isShow:隐私权政策是否弹窗展示告知用户;true是展示
     */
    void updatePrivacyShow(boolean isContains,boolean isShow);

    /**
     * 设置是否同意用户授权政策
     */
    void updatePrivacyAgree(boolean isAgree);
}
