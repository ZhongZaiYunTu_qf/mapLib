package com.zz.yt.lib.map.delegate.selection.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.zz.yt.lib.map.R;
import com.zz.yt.lib.map.delegate.selection.entity.MapListEntity;

import java.util.List;


/**
 * @author gc
 * @version 21-09-08
 */
public class MapListAdapter extends RecyclerView.Adapter<MapListAdapter.Holder> {

    private List<MapListEntity> list;
    private OnItemClickListener mOnItemClickListener;

    public MapListAdapter(List<MapListEntity> listBeans) {
        this.list = listBeans;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ////解决宽度不能铺满
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.hai_map_item_address, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, final int position) {
        String title, distance;
        if (position == 0) {
            title = "地图位置";
            distance = list.get(position).getAddress();
        } else {
            title = list.get(position).getName();
            distance = list.get(position).getDistance() + "m | " + list.get(position).getAddress();
        }

        holder.title.setText(title);
        holder.distance.setText(distance);
        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeSelect(position);
                mOnItemClickListener.onItemClick(position);
            }
        });

        if (list.get(position).isSelect()) {
            holder.check.setVisibility(View.VISIBLE);
        } else {
            holder.check.setVisibility(View.GONE);
        }
    }

    private void changeSelect(int position) {
        if (list.size() + 1 >= position) {
            for (MapListEntity listBean : list) {
                listBean.setSelect(false);
            }

            list.get(position).setSelect(true);

            notifyDataSetChanged();
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public List<MapListEntity> getData() {
        return list;
    }

    public void setList(List<MapListEntity> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    public static class Holder extends RecyclerView.ViewHolder {

        private final TextView title, distance;
        private final LinearLayout linearLayout;
        private final ImageView check;

        public Holder(View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.tv_title);
            distance = itemView.findViewById(R.id.tv_distance);
            linearLayout = itemView.findViewById(R.id.ll_content);
            check = itemView.findViewById(R.id.check);
        }
    }

    // 自定义点击事件
    public void setOnItemClickListener(OnItemClickListener mOnItemClickListener) {
        this.mOnItemClickListener = mOnItemClickListener;
    }

    public interface OnItemClickListener {
        void onItemClick(int position);

    }

}
