package com.zz.yt.lib.map.checker;


import androidx.annotation.NonNull;

import com.whf.android.jar.util.storage.LattePreference;


/**
 * @author qf
 * @version 2021/11/4
 */

public class MapManager {

    private enum MapTag {
        /**
         * 是隐私权政策是否包含高德开平隐私权政策  true是包含
         */
        CONTAINS_TAG,

        /**
         * 隐私权政策是否弹窗展示告知用户 true是展示
         */
        SHOW_TAG,

        /**
         * 隐私权政策是否取得用户同意;true是用户同意
         */
        AGREE_TAG
    }

    //region 登录状态

    /**
     * @param isContains:是隐私权政策是否包含高德开平隐私权政策;rue是包含
     * @param isShow:隐私权政策是否弹窗展示告知用户;true是展示
     */
    public static void setMapContains(boolean isContains, boolean isShow) {
        LattePreference.setAppFlag(MapTag.CONTAINS_TAG.name(), isContains);
        LattePreference.setAppFlag(MapTag.SHOW_TAG.name(), isShow);
    }

    /**
     * @param isAgree:隐私权政策是否取得用户同意;true是用户同意
     */
    public static void setMapAgree(boolean isAgree) {
        LattePreference.setAppFlag(MapTag.AGREE_TAG.name(), isAgree);
    }

    private static boolean isMapContains() {
        return LattePreference.getAppFlag(MapTag.CONTAINS_TAG.name());
    }

    private static boolean isMapShow() {
        return LattePreference.getAppFlag(MapTag.SHOW_TAG.name());
    }

    private static boolean isMapAgree() {
        return LattePreference.getAppFlag(MapTag.AGREE_TAG.name());
    }


    /**
     * 实例化之前调用
     *
     * @param checker:设置包含隐私政策，并展示用户授权弹窗
     */
    public static void updatePrivacy(@NonNull IMapChecker checker) {
        checker.updatePrivacyShow(isMapContains(), isMapShow());
        checker.updatePrivacyAgree(isMapAgree());
    }

    //endregion


}
