package com.zz.yt.lib.map.colorpicker;

/**
 * 颜色回调
 */
public interface OnColorBarListener {

    void moveBar(int color);
}
