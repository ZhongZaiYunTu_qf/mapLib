package com.zz.yt.lib.map.bean;

import java.util.List;

public class WeatherBean {


    /**
     * city : 重庆市
     * adcode : 500000
     * province : 重庆
     * reporttime : 2021-12-20 10:33:43
     * casts : [{"date":"2021-12-20","week":"1","dayweather":"多云","nightweather":"晴","daytemp":"13","nighttemp":"7","daywind":"东","nightwind":"东","daypower":"≤3","nightpower":"≤3"},{"date":"2021-12-21","week":"2","dayweather":"晴","nightweather":"晴","daytemp":"14","nighttemp":"7","daywind":"东","nightwind":"东","daypower":"≤3","nightpower":"≤3"},{"date":"2021-12-22","week":"3","dayweather":"晴","nightweather":"阴","daytemp":"15","nighttemp":"6","daywind":"北","nightwind":"北","daypower":"≤3","nightpower":"≤3"},{"date":"2021-12-23","week":"4","dayweather":"小雨","nightweather":"中雨","daytemp":"12","nighttemp":"10","daywind":"西北","nightwind":"西北","daypower":"≤3","nightpower":"≤3"}]
     */

    private String city;
    private String adcode;
    private String province;
    private String reporttime;
    /**
     * date : 2021-12-20
     * week : 1
     * dayweather : 多云
     * nightweather : 晴
     * daytemp : 13
     * nighttemp : 7
     * daywind : 东
     * nightwind : 东
     * daypower : ≤3
     * nightpower : ≤3
     */

    private List<CastsBean> casts;

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAdcode() {
        return adcode;
    }

    public void setAdcode(String adcode) {
        this.adcode = adcode;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getReporttime() {
        return reporttime;
    }

    public void setReporttime(String reporttime) {
        this.reporttime = reporttime;
    }

    public List<CastsBean> getCasts() {
        return casts;
    }

    public void setCasts(List<CastsBean> casts) {
        this.casts = casts;
    }

    public static class CastsBean {
        private String date;
        private String week;
        private String dayweather;
        private String nightweather;
        private String daytemp;
        private String nighttemp;
        private String daywind;
        private String nightwind;
        private String daypower;
        private String nightpower;

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getWeek() {
            return week;
        }

        public void setWeek(String week) {
            this.week = week;
        }

        public String getDayweather() {
            return dayweather;
        }

        public void setDayweather(String dayweather) {
            this.dayweather = dayweather;
        }

        public String getNightweather() {
            return nightweather;
        }

        public void setNightweather(String nightweather) {
            this.nightweather = nightweather;
        }

        public String getDaytemp() {
            return daytemp;
        }

        public void setDaytemp(String daytemp) {
            this.daytemp = daytemp;
        }

        public String getNighttemp() {
            return nighttemp;
        }

        public void setNighttemp(String nighttemp) {
            this.nighttemp = nighttemp;
        }

        public String getDaywind() {
            return daywind;
        }

        public void setDaywind(String daywind) {
            this.daywind = daywind;
        }

        public String getNightwind() {
            return nightwind;
        }

        public void setNightwind(String nightwind) {
            this.nightwind = nightwind;
        }

        public String getDaypower() {
            return daypower;
        }

        public void setDaypower(String daypower) {
            this.daypower = daypower;
        }

        public String getNightpower() {
            return nightpower;
        }

        public void setNightpower(String nightpower) {
            this.nightpower = nightpower;
        }
    }
}
